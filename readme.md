# scripts - Practical Computing for Biologists

This repository considers a collection of scripts that I wrote and edited as I read through the book *Practical Computing for Biologists*

These scripts do a variety of things, including:

-Analyze test DNA sequences

-Manipulate latitudes and longitudes
